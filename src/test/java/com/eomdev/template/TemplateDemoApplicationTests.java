package com.eomdev.template;

import com.eomdev.template.config.DatabaseProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.Entity;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TemplateDemoApplication.class)
public class TemplateDemoApplicationTests {

	@Autowired
	private Environment environment;

	@Autowired
	private DatabaseProperties databaseProperties;

	@Test
	public void contextLoads() {



	}

}
