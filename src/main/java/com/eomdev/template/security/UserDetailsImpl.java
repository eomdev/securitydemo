package com.eomdev.template.security;

import com.eomdev.template.account.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by eomdev on 2016. 5. 19..
 */
public class UserDetailsImpl implements UserDetails {

    private Account account;

    public UserDetailsImpl(Account account) {
        this.account = account;
    }

    @Override   // 계정이 가지고 있는 권한 목록
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add((new SimpleGrantedAuthority(account.getRoleType().name())));
        return authorities;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getEmail();
    }

    @Override   // 계정이 만료되진 않았는가
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override   // 계정이 잠기지 않았는가
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override   // 계정의 패스워드가 만료되지 않았는지
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override   // 사용 가능한 계정인지
    public boolean isEnabled() {    // 사용이 가능한가
        return true;
    }
}
