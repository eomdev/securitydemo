package com.eomdev.template.web;

import com.eomdev.template.account.Account;
import com.eomdev.template.account.AccountDto;
import com.eomdev.template.account.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by eomdev on 2016. 5. 20..
 */
@RestController
public class AdminController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * 관리자가 전체 유저의 정보를 가져온다.
     * 패스워드는 노출시키지 않는다.
     * @return
     */
    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public ResponseEntity getUsers(){

        List<Account> accounts = accountService.findAll();

        List<AccountDto.Response> responses = accounts.parallelStream()
                .map(account -> modelMapper.map(account, AccountDto.Response.class))
                .collect(Collectors.toList());

        return new ResponseEntity(responses, HttpStatus.OK);

    }

}
