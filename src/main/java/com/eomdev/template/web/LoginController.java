package com.eomdev.template.web;

import com.eomdev.template.account.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by eomdev on 2016. 5. 20..
 */
@RestController
public class LoginController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private AccountService userService;

    @RequestMapping("/loginSuccess")
    public ResponseEntity loginSuccess(Principal account){
        return new ResponseEntity<>(account, HttpStatus.OK);
    }


    @RequestMapping("/logoutSuccess")
    public ResponseEntity logoutSuccess(){
        return new ResponseEntity(HttpStatus.OK);
    }



}
