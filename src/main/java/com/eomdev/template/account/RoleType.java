package com.eomdev.template.account;

/**
 * Created by eomdev on 2016. 4. 21..
 */
public enum RoleType {
    ROLE_USER, ROLE_ADMIN
}
