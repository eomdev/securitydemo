package com.eomdev.template.account;

/**
 * Created by eomdev on 2015. 9. 12..
 */
public class AccountNotFoundException extends RuntimeException{

    Long id;

    public AccountNotFoundException(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}

