package com.eomdev.template.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by eomdev on 2016. 4. 20..
 */
@Entity
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue
    @Column(name = "ACCOUNT_ID", updatable = false)
    private Long id;

    private String email;

    @JsonIgnore
    private String password;

    private String name;

    private LocalDateTime joined;
    private LocalDateTime updated;

    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    public Account(String email, String password, String name, LocalDateTime joined, RoleType roleType) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.joined = joined;
        this.roleType = roleType;
    }
}

