package com.eomdev.template.account;

/**
 * Created by eomdev on 2016. 5. 19..
 */
public class AccountDuplicatedEmailException extends RuntimeException {

    String username;

    public AccountDuplicatedEmailException(String message, String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

}
