package com.eomdev.template.account;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by eomdev on 2016. 4. 20..
 */
public interface AccountRepository extends JpaRepository<Account, Long>{
    Account findByEmail(String email);
}
