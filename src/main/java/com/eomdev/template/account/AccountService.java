package com.eomdev.template.account;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by eomdev on 2016. 5. 19..
 */
@Service
@Slf4j
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper modelMapper;

    public Account createAccount(AccountDto.Create dto){

        Account account = modelMapper.map(dto, Account.class);

        // 이미 가입된 유저가 있는지 검사
        String email = dto.getEmail();
        if(existUser(email)){
            log.error("account duplicated email exception. {}", email);
            throw new AccountDuplicatedEmailException("already registered email address", email);
        }

        // 패스워드 암호화
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setJoined(LocalDateTime.now());
        account.setRoleType(RoleType.ROLE_USER);

        return accountRepository.save(account);

    }

    public List<Account> findAll(){
        return accountRepository.findAll();
    }

    public boolean existUser(String email){
        Account account = findByEmail(email);
        return account != null ? true : false;
    }

    public Account findByEmail(String email){
        return accountRepository.findByEmail(email);
    }

    public Account getAccount(Long id){
        return accountRepository.findOne(id);
    }


    public void deleteAccount(Long id) {
        accountRepository.delete(id);
    }

    public Account updateAccount(Long id, AccountDto.Update updateDto) {
        Account account = getAccount(id);
        if(account == null){
            throw new AccountNotFoundException(id);
        }
        account.setPassword(passwordEncoder.encode(updateDto.getPassword()));
        account.setName(updateDto.getName());
        account.setUpdated(LocalDateTime.now());
        return accountRepository.save(account);
    }

}