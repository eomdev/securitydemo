package com.eomdev.template.account;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

/**
 * Created by eomdev on 2016. 5. 19..
 */
public class AccountDto {

    @Data
    public static class Create{

        @NotBlank
        @Email
        private String email;

        @NotBlank
        private String password;

        @NotBlank
        private String name;

    }

    @Data
    public static class Update{

        @NotBlank
        private String password;

        @NotBlank
        private String name;
    }


    @Data
    public static class Response{
        private Long id;
        private String email;
        private String name;
        private LocalDateTime joined;
        private LocalDateTime updated;

        @Enumerated(EnumType.STRING)
        private RoleType roleType;

    }


}
