package com.eomdev.template.util.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.metamodel.Attribute;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by eomdev on 2016. 4. 21..
 * http://blog.eomdev.com/java/2016/01/04/jpa_with_java8.html
 * Java8의 날짜 API를 사용하기 위해서 관련 클래스를 추가
 * Java8이 릴리즈 되기 전에 JPA 2.1이 나왔기 때문에 JPA2.1이 Jaava8의 날짜와 시간 API를 지원하지 못하는 문제를 해결하기 위한 클래스
 * LocalDateTime과 Timestamp를 서로 변환시키는 converter 역할을 한다.
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp>{

    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime localDateTime) {
        return (localDateTime == null ? null : Timestamp.valueOf(localDateTime));
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp timestamp) {
        return (timestamp == null ? null : timestamp.toLocalDateTime());
    }
}
