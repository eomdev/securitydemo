package com.eomdev.template.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by eomdev on 2016. 5. 21..
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()   // ApiSelectorbuilder 인스턴스를 리턴 : 스웨거에 의해 노출되는 endpoint를 제어하는 방법을 제공해준다.
                .apis(RequestHandlerSelectors.any())    // Request Handler
                .paths(PathSelectors.any()) // PathSelect
                .build();
    }

}
