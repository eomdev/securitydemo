package com.eomdev.template.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by eomdev on 2015. 8. 27..
 */
@Configuration
public class DataConfig {

    @Autowired
    private DatabaseProperties databaseProperties;

    @Bean
    public DataSource dataSource(){

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(databaseProperties.getUrl());
        config.setUsername(databaseProperties.getUsername());
        config.setPassword(databaseProperties.getPassword());
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        HikariDataSource dataSource = new HikariDataSource(config);

        return dataSource;
    }

    @Bean
    public JpaTransactionManager transactionManager(){
        return new JpaTransactionManager();
    }

    // LocalContainerEntityManagerFactoryBean : JPA를 스프링 컨테이너에서 사용할 수 있도록 스프링프레임워크가 제공하는 기능
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan("com.eomdev.template"); // @Entity 탐색위치
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());   // JPA 구현체로는 하이버네이트를 사용
//        factoryBean.setSharedCacheMode(SharedCacheMode.ENABLE_SELECTIVE);   // 2차 캐쉬 모드 설정 SharedCacheMode.ENABLE_SELECTIVE : @Cacheable(true)로 설정된 엔티티만 캐시를 사용한다.


        // 상세설정
        Properties jpaProperties = new Properties();
        jpaProperties.put(AvailableSettings.SHOW_SQL, true);    // 실행한 SQL 보기
        jpaProperties.put(AvailableSettings.FORMAT_SQL, true);  // 실행한 SQL 정렬해서 보기
        jpaProperties.put(AvailableSettings.USE_SQL_COMMENTS, true);    // SQL 코멘트 보기
        jpaProperties.put(AvailableSettings.HBM2DDL_AUTO, databaseProperties.getHbm2ddl());  // DDL 자동생성
        jpaProperties.put(AvailableSettings.DIALECT, "org.hibernate.dialect.H2Dialect");    // 방언 설정    TODO : yml 설정파일로 변경할 것
        jpaProperties.put(AvailableSettings.USE_NEW_ID_GENERATOR_MAPPINGS, true);   // 새 버젼의 ID생성 옵션
        jpaProperties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        factoryBean.setJpaProperties(jpaProperties);



        return factoryBean;

    }

}
