package com.eomdev.template.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * Created by eomdev on 2016. 5. 19..
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    private AuthenticationFailureHandler restLoginFailureHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .exceptionHandling()
                    .authenticationEntryPoint(restAuthenticationEntryPoint)
            .and()
                .formLogin()
//                    .loginPage("/loginFailure")            // 로그인form 페이지의 url을 설정
                    .loginProcessingUrl("/login") // 로그인을 처리하는 url
                    .defaultSuccessUrl("/loginSuccess", true)   // 로그인 성공시 redirect되는 곳, true : 원래 요청했던 페이지로 가지 않는다.
                    .usernameParameter("email")
                    .passwordParameter("pwd")
                    .failureUrl("/loginFailure")
//                    .successHandler(new CustomAuthenticationSuccessHandler())
                    .failureHandler(restLoginFailureHandler) // 로그인 실패시 해당클래스로 이동하여 실패시에 대한 처리를 따로함(로그 남기기 해당 계정명의 로그인 실패횟수 올리기 등등...)
            .and()
                .logout()
//                .deleteCookies("SESSION")
                    .logoutUrl("/logout")   // 로그아웃 url
                    .logoutSuccessUrl("/logoutSuccess").permitAll() // 로그아웃이 성공적으로 처리되고, redirect되는 곳
            .and()
                .authorizeRequests()
                    .antMatchers("/login","/loginFailure").permitAll()  // 모두 접근 가능하도록 설정
                    .antMatchers(HttpMethod.POST, "/account").permitAll()
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest().hasAnyRole("ADMIN", "USER");
//                authenticated();  // 나머지는 인증이 필요하다

        // 스웨거 관련 api : http://localhost:8080/appName/v2/ap-docs
        // 스웨거 UI : http://localhost:8080/apapName/swagger-ui.html

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }



    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }





}
