package com.eomdev.template.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by eomdev on 2015. 10. 1..
 */
@ConfigurationProperties(prefix = "db")
@Component
@Data
public class DatabaseProperties {

    private String driver;
    private String url;
    private String username;
    private String password;
    private String hbm2ddl;


}
