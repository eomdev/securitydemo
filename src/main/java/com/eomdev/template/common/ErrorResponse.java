package com.eomdev.template.common;

import lombok.Data;

import java.util.List;

/**
 * Created by eomdev on 2016. 5. 21..
 */
@Data
public class ErrorResponse {

    private String message;
    private String code;
    private List<FieldError> errors;

    // TODO
    public static class FieldError{
        private String field;
        private String value;
        private String reason;
    }



}